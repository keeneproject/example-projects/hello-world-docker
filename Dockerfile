FROM ubuntu:24.04
#FROM ubuntu:latest
#FROM debian:stable-slim


# Set environment variable. So python output to stdout doesnt get jumbled.
ENV PYTHONUNBUFFERED=1


# Install c compiler and python
RUN apt-get update && apt-get -y upgrade && \
    apt-get -y install gcc python3

# Create non-root user
RUN useradd runuser

# Create WORKDIR, cp in code, change owner
RUN mkdir /opt/helloworld-app/
COPY testscript.sh hello.c mypythonapp.py /opt/helloworld-app/
RUN chown -R runuser:runuser /opt/helloworld-app/

# Change to non-root and move to WORKDIR
USER runuser
WORKDIR /opt/helloworld-app

# Compile my C program which is used in our mypythonapp.py program
RUN gcc -o ./hello ./hello.c

# Delete C source code and fix permissions
RUN rm -f ./hello.c
RUN chmod 700 ./hello
RUN chmod 700 ./testscript.sh
RUN chmod 600 ./mypythonapp.py

# Swith to root and autoremove gcc to remove gcc and unneeded packages
USER root
RUN apt -y autoremove gcc

# Switch back to non-root user, so the container runs as non-root user
USER runuser

# The command we want our container which is our mypythonapp.py app
CMD [ "python3", "./mypythonapp.py"]
